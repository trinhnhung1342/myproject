<html>
<head>
    <meta charset="utf-8">
    <title>Trang quản trị</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<?php
session_start();
if(!isset($_SESSION['login'])){
    header('location:login.php');
}
?>
<body>
<div id="wrapper">
    <?php
    include('modules/config.php');
    include('modules/header.php');
    include('modules/menu.php');
    include('modules/content.php');
    include('modules/footer.php');
    ?>
</div>
</body>
</html>