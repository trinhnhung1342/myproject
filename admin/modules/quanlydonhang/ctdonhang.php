<?php
$id = $_GET['id'];
$sql = "select * from ctdonhang,sanpham where iddonhang=$id and ctdonhang.idsanpham=sanpham.idsanpham";
$run = mysqli_query($connection,$sql);
?>
    <div style="">
        <table border="1" width="100%">
            <tr>
                <th>ID đơn hàng</th>
                <th>ID sản phẩm</th>
                <th>Tên sản phẩm</th>
                <th>Hình ảnh</th>
                <th>Số lượng</th>
                <th>Tổng tiền</th>
            </tr>
            <?php
            $tongtien = 0;
            while($dong = mysqli_fetch_array($run)) {
            ?>
            <tr>
                <td>Đơn hàng <?php echo $dong['iddonhang'] ?></td>
                <td><?php echo $dong['idsanpham'] ?></td>
                <td><?php echo $dong['tensp'] ?></td>
                <td><img src="modules/quanlysanpham/uploads/<?php echo $dong['hinhanh'] ?>" width="60" height="60"></td>
                <td><?php echo $dong['Soluong'] ?></td>
                <td><?php echo number_format($dong['tongtien'],0,'','.') ?>Đ</td>
            </tr>

                <?php
                $tongtien += $dong['tongtien'];
            }
            ?>
            <tr>
                <td colspan="6">Tổng tiền: <?php echo number_format($tongtien,0,'','.') ?>Đ</td>
            </tr>
        </table>
    </div>
