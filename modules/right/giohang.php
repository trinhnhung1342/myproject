<?php
echo '<div>';
if(!isset($_SESSION['dangnhap']) or empty($_SESSION['dangnhap'])){
    echo '<div style="height: 500px;"> Bạn chưa đăng nhập. Mời bạn <a href="index.php?quanly=dangnhap">Đăng nhập</a> hoặc <a href="index.php?quanly=dangky">Đăng ký</a> nếu chưa có tài khoản</div>';
}else{
    $sql="select * from sanpham";
    $query=mysqli_query($connection,$sql);
    if(!isset($_SESSION['cart']) or empty($_SESSION['cart'])){
        echo '<div style="height: 500px;">Chưa có sản phẩm trong giỏ hàng</div>';
    }else {
        $thanhtien = 0;
        $tongtien = 0;
        echo '<div class="tieude">Giỏ hàng</div>';
        echo '<br/>';
        echo '<form action="updatecart.php" method="post">';
        echo '<table border="1" width="98%" style="text-align: center">';
        echo '<tr>';
        echo '<th height="40">Tên sản phẩm</th>';
        echo '<th>Hình ảnh</th>';
        echo '<th>Giá</th>';
        echo '<th>Số lượng</th>';
        echo '<th>Thành tiền</th>';
        echo '<th>Xóa</th>';
        echo '</tr>';
        foreach ($_SESSION['cart'] as $value) {
            $thanhtien = $value['giaca'] * $value['quanlity'];
            $id = $value['idsanpham'];
            echo '<tr>';
            echo '<td height="40">' . $value['tensp'] . '</td>';
            echo '<td><img src="admin/modules/quanlysanpham/uploads/' . $value['hinhanh'] . '" width="200" height="200" /></td>';
            echo '<td>' . number_format($value['giaca'], 0, '', '.') . 'Đ</td>';
            if ($value['quanlity'] <= $value['soluong']) {
                echo '<td><input type="text" name="' . $id . '" value="' . $value['quanlity'] . '" </td>';
                echo '<td>' . number_format($thanhtien, 0, '', '.') . 'Đ</td>';
            } else {
                echo '<p style="font-size:16px; color:red;">Số lượng sản phẩm ' . $value['tensp'] . ' không vượt quá ' . $value['soluong'] . '</p>';
                echo '<td><input type="text" name="' . $id . '" value="' . $value['soluong'] . '"/> </td>';
                echo '<td>' . number_format($value['giaca'] * $value['soluong'], 0, '', '.') . 'Đ</td>';
                $thanhtien=$value['giaca']*$value['soluong'];
            }
            echo '<td><a href="deletecart.php?id=' . $value['idsanpham'] . '" style="text-decoration: none; color: #1420b6">Xóa</td>';
            echo '</tr>';
            $tongtien += $thanhtien;
        }
        echo '<tr>';
        echo '<td colspan="6" height="40">Tổng tiền: ' . number_format($tongtien, 0, '', '.') . 'VNĐ</td>';
        echo '</tr>';
        echo '</table>';
        echo '<br/>';
        echo '<div align="center"><input type="submit" name="update" value="Update" style="width: 100px; background: #c30307; height: 30px; color: #fff"></div>';
        echo '</form>';
        echo '<div class="thongtin" align="center">';
        echo '<h3>Thông tin đơn hàng</h3>';
        echo '<form action="?quanly=thanhtoan" method="post">';
        echo '<table style="margin-left: 10px">';
        echo '<tr>';
        foreach ($_SESSION['cart'] as $value) {
            echo '<td>Tên sản phẩm:</td>';
            echo '<td><input type="text" name="idsanpham" readonly="" value="' . $value['tensp'] . '"></td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>Số lượng:</td>';
            echo '<td><input type="text" name="soluong" readonly="" value="' . $value['quanlity'] . '"></td>';
            echo '</tr>';
            echo '<tr>';
        }
        echo '<td>Số tiền:</td>';
        echo '<td><input type="text" name="thanhtien" readonly="" value="'. number_format($tongtien,0,'','.').'Đ"></td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td><input type="submit" name="thanhtoan" value="Thanh toán" style="width: 100px; background: #c30307; height: 30px; color: #fff"></td>';
        echo '</tr>';
        echo '</table>';
        echo '</form>';
        echo '</div>';
    }
}
echo '</div>';
?>

